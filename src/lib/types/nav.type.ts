export type NavItem = {
	name: string;
	path: string;
	icon: string;
};
