import type { NavItem } from '$lib/types/nav.type';

const navItems: NavItem[] = [
	{
		name: 'Home',
		path: '/',
		icon: 'home'
	},
	{
		name: 'Games',
		path: '/games',
		icon: 'folder'
	},
	{
		name: 'Saved',
		path: '/saved',
		icon: 'bookmark'
	},
	{
		name: 'Settings',
		path: '/settings',
		icon: 'settings'
	}
];

export default navItems;
